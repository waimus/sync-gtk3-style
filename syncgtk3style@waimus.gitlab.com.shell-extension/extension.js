/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import {QuickToggle, SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

const ExtensionIndicator = GObject.registerClass(
    class ExtensionIndicator extends SystemIndicator {
        _init(extensionObject) {
            super._init();

            this._indicator = this._addIndicator();
            this._indicator.icon_name = 'selection-mode-symbolic';

            this._settings = new Gio.Settings({
                schema_id: 'org.gnome.desktop.interface',
            });
            this._settings.connect('changed::color-scheme', () => this._sync());

            this.connectObject('destroy', () => this._settings.run_dispose(), this);
            this._sync();
        }

        _sync() {
            const colorScheme = this._settings.get_string('color-scheme');
            const checked = colorScheme === 'prefer-dark';

            if (this.checked !== checked) {
                this.set({checked});
            }

            this._settings.set_string('gtk-theme', this.checked ? 'adw-gtk3-dark' : 'adw-gtk3');
            this._indicator.icon_name = this.checked ? 'weather-clear-night-symbolic' : 'weather-clear-symbolic';
        }
    });

export default class SyncGtk3StyleExt extends Extension {
    enable() {
        this._indicator = new ExtensionIndicator();
        Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);
    }

    disable() {
        this._indicator.quickSettingsItems.forEach(item => item.destroy());
        this._indicator.destroy();
        this._indicator = null;
    }
}
