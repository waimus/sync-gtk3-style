# Sync GTK3 theme to preferred color scheme in GNOME Shell

This was a similar extension to my other extension "colorprefs-toggle" but this extension is simply listening to the "changed::color-scheme" signal, and then apply additional stuff. Whereas `colorprefs-toggle` also provides a button and has its own action when the button is pressed. But having two buttons (the built-in dark mode button and my extension) is redundant. I eventually think of making this instead.

## Note :/
- Requires the [adw-gtk](https://github.com/lassekongo83/adw-gtk3) theme to be installed. Otherwise you could specifi which GTK3 theme to use in the `extension.js`

# Attribution
I adapted the code from [Florian Müllner's Quick Settings Extension](https://gitlab.gnome.org/fmuellner/quick-settings-extension/-/blob/main/status/darkMode.js) and [GJS Extension documentation](https://gjs.guide/extensions/topics/quick-settings.html#basic-toggle) to figure out stuff and make this.

Additionally the base idea is inspired from [my older bash script implementation](https://gitlab.com/waimus/dotfiles/-/blob/main/desktop-home/.local/bin/colorprefs.sh) to toggle these GSettings.
 
# License
GNU General Public License version 2.0 or later.